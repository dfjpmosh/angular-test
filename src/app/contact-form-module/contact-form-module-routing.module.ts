import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactFormComponentComponent } from './contact-form-component/contact-form-component.component';

const routes: Routes = [
  {
    path: '',
    component: ContactFormComponentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactFormModuleRoutingModule { }
