import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactFormModuleRoutingModule } from './contact-form-module-routing.module';
import { ContactFormComponentComponent } from './contact-form-component/contact-form-component.component';
import { ShareModule } from '../core/share/share.module';


@NgModule({
  declarations: [
    ContactFormComponentComponent
  ],
  imports: [
    CommonModule,
    ContactFormModuleRoutingModule,
    ShareModule
  ]
})
export class ContactFormModuleModule { }
