import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contact-form-component',
  templateUrl: './contact-form-component.component.html',
  styleUrls: ['./contact-form-component.component.css']
})
export class ContactFormComponentComponent implements OnInit {

  form: FormGroup;

  constructor(private _fb: FormBuilder) { 
    this.form = this._fb.group({
      nombre: [null, [Validators.required, Validators.pattern("^[A-Za-z ]+$")]],
      correo: [null, [Validators.required, Validators.email]],
      telefono: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]+$")]],
      mensaje: [null, [Validators.required, Validators.maxLength(40)]]
    });
  }

  ngOnInit(): void {
  }

  onEnviar(): void{
 
    console.log(this.form.value);

    if(this.form.invalid){
      this.form.markAllAsTouched();
      return;
    }        

    Swal.fire({  icon: 'success',  title: "Mesaje enviado",  text: "Nombre: " + this.form.value.nombre + '\n' +
                                                                      "Correo: " + this.form.value.correo + '\n' +
                                                                      "Telefono: " + this.form.value.telefono + '\n' +
                                                                      "Mensaje: " + this.form.value.mensaje});

  }

  get nombre(): FormControl{
    return <FormControl>this.form.get('nombre');
  }

  get correo(): FormControl{
    return <FormControl>this.form.get('correo');
  }

  get telefono(): FormControl{
    return <FormControl>this.form.get('telefono');
  }

  get mensaje(): FormControl{
    return <FormControl>this.form.get('mensaje');
  }

}
