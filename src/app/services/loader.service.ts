import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  isloading =  new BehaviorSubject(false);

  constructor() { }

  show(): void{
    this.isloading.next(true);
  }

  hide(): void{
    this.isloading.next(false);
  }
}
