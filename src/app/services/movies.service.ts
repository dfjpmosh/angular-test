import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient	 } from '@angular/common/http';
import { Observable } from 'rxjs';
import { movie } from '../core/interfaces/movie';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  url: string = environment.url;

  constructor(private _http: HttpClient) { }

  getMovies(): Observable<movie[]>{
    return this._http.get<movie[]>(this.url);
  }
}
