import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { movie } from 'src/app/core/interfaces/movie';
import { MoviesService } from 'src/app/services/movies.service';
import { MatPaginator } from '@angular/material/paginator';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-vista1',
  templateUrl: './vista1.component.html',
  styleUrls: ['./vista1.component.css']
})

export class Vista1Component implements OnInit {

  displayedColumns: string[] = ['title', 'year', 'cast', 'genres'];
  dataSource = new MatTableDataSource<movie>();

  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _moviesService: MoviesService, private _loaderService: LoaderService) { }

  ngOnInit(): void {
    this.onGetMovies();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  onGetMovies(): void {
    this._loaderService.show();
    this._moviesService.getMovies().subscribe(
      data => {
        this.dataSource = new MatTableDataSource<movie>(data);
        this.dataSource.paginator = this.paginator;
        this._loaderService.hide();
      },
      error => {
        console.log(error);        
      }
    );
  }

}
