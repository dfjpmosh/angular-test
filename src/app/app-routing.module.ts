import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Vista1Component } from './components/vista1/vista1.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  {
    path:'',
    component: Vista1Component
  },
  {
    path:'vista1',
    component: Vista1Component
  },
  {
    path: 'contacto',
    loadChildren:() => import("./contact-form-module/contact-form-module.module").then(m => m.ContactFormModuleModule)
  },
  {
    path: '**',
    pathMatch: 'full',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
