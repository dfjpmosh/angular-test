import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service'; 

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  showProgress: BehaviorSubject<boolean> = this._loaderservice.isloading;

  constructor(private _loaderservice: LoaderService) { }

  ngOnInit(): void {
  }

}
